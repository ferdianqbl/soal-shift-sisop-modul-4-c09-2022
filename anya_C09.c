#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <stdbool.h>

static const char *dirPath = "/home/ferdianqbl/Documents/test";

void logForWibu(const char *lama, char *baru, int tipe)
{
  char logPath[100];
  char *logName = "Wibu.log";
  sprintf(logPath, "%s/%s", dirPath, logName);
  FILE *logFile = fopen(logPath, "a");

  if (tipe == 0)
    fprintf(logFile, "MKDIR\tterenkripsi\t%s\t->\t%s\n", lama, baru);
  else if (tipe == 1)
    fprintf(logFile, "RENAME\tterenkripsi\t%s\t->\t%s\n", lama, baru);
  else if (tipe == 2)
    fprintf(logFile, "RENAME\tterdecode\t%s\t->\t%s\n", lama, baru);
  fclose(logFile);
}

void encodeForWibu(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
    return;
  for (int i = 0; i < strlen(str); i++)
  {
    if (str[i] == '/')
      continue;
    if (str[i] == '.')
      break;

    if (str[i] >= 'A' && str[i] <= 'Z')
      str[i] = 'Z' + 'A' - str[i];
    if (str[i] >= 'a' && str[i] <= 'z')
      str[i] = 'a' + (str[i] - 'a' + 13) % 26;
  }
}

void decodeForWibu(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL)
    return;
  int s = 0, length = strlen(str);
  for (int i = length; i >= 0; i--)
  {
    if (str[i] == '/')
      break;
    if (str[i] == '.')
    {
      length = i;
      break;
    }
  }
  for (int i = 0; i < length; i++)
  {
    if (str[i] == '/')
    {
      s = i + 1;
      break;
    }
  }
  for (int i = s; i < length; i++)
  {
    if (str[i] == '/')
      continue;
    if (str[i] >= 'A' && str[i] <= 'Z')
      str[i] = 'Z' + 'A' - str[i];
    if (str[i] >= 'a' && str[i] <= 'z')
      str[i] = 'a' + (str[i] - 'a' + 13) % 26;
  }
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *tempStr;
  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }

  char fpath[1000];

  sprintf(fpath, "%s%s", dirPath, path);
  int res = lstat(fpath, stbuf);

  if (res == -1)
    return -errno;

  return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char *tempStr;
  int flag;

  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    flag = 1;
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirPath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirPath, path);

  int result = 0;
  DIR *dp;
  struct dirent *dir;
  (void)offset;
  (void)fi;

  dp = opendir(fpath);
  if (dp == NULL)
    return -errno;

  while ((dir = readdir(dp)) != NULL)
  {
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = dir->d_ino;
    st.st_mode = dir->d_type << 12;
    if (tempStr != NULL)
    {
      if (flag == 1)
        encodeForWibu(dir->d_name);
    }
    result = (filler(buf, dir->d_name, &st, 0));
    if (result != 0)
      break;
  }
  closedir(dp);
  return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
  if (strstr(path, "Animeku_") != NULL)
  {
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
      path = dirPath;
      sprintf(fpath, "%s", path);
    }
    else
      sprintf(fpath, "%s%s", dirPath, path);
    int result = mkdir(fpath, mode);
    char *folderPath = strstr(path, "Animeku_");
    if (folderPath != NULL)
      logForWibu(fpath, fpath, 0);
    if (result == -1)
      return -errno;
  }
  return 0;
}

static int xmp_rename(const char *sumber, const char *tujuan)
{
  int flag;
  if (strstr(sumber, "Animeku_") != NULL)
    flag = 1;

  char filesrc[1000], filegoal[1000];
  sprintf(filesrc, "%s%s", dirPath, sumber);
  sprintf(filegoal, "%s%s", dirPath, tujuan);
  int result;
  if (flag == 1)
  {
    if (strstr(filegoal, "Animeku_") != NULL)
      logForWibu(filesrc, filegoal, 1);
    else if (strstr(filesrc, "Animeku_") != NULL)
      logForWibu(filesrc, filegoal, 2);
    result = rename(filesrc, filegoal);
  }

  if (result == -1)
    return -errno;
  return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
  char *tempStr;
  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }
  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirPath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirPath, path);

  int result;
  result = open(fpath, fi->flags);
  if (result == -1)
    return -errno;
  close(result);
  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char *tempStr;
  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirPath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirPath, path);

  int result = 0;
  int fd = 0;
  (void)fi;

  fd = open(fpath, O_RDONLY);
  if (fd == -1)
    return -errno;

  result = pread(fd, buf, size, offset);
  if (result == -1)
    result = -errno;

  close(fd);
  return result;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char *tempStr;
  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirPath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirPath, path);

  int result;
  int fd;
  (void)fi;

  fd = open(fpath, O_WRONLY);
  if (fd == -1)
    return -errno;

  result = pwrite(fd, buf, size, offset);
  if (result == -1)
    result = -errno;
  close(fd);
  return result;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .read = xmp_read,
    .open = xmp_open,
    .write = xmp_write,
};

int main(int argc, char *argv[])
{
  umask(0);
  return fuse_main(argc, argv, &xmp_oper, NULL);
}
