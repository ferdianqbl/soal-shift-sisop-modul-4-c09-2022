# Kelompok C09

- 5025201165 - Gabriel Solomon Sitanggang
- 5025201102 - Arya Nur Razzaq
- 5025201020 - Muhammad Ferdian Iqbal
  <br><br>


# Fungsi Utama
- Main
``` C++
int main(int argc, char *argv[])
{
  umask(0);
  return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
> Fungsi ini sebagai awalan program yang akan dijalankan

- Struct Operand
``` C++
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .read = xmp_read,
    .open = xmp_open,
    .write = xmp_write,
};
```
> Struct ini berisi pointer yang menuju ke fungsi yang ditunjuk

# Soal 1

## Persoalan
- Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
	
	Contoh : 
	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”
- Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
- Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
- Setiap data yang terencode akan masuk dalam file “Wibu.log” 
**Contoh isi:**
**RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat** 
**RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba**
- Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)
**Note : filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode**

Referensi : https://www.base64encode.org/ https://rot13.com/

## Penyelesaian
- Fungsi getattr
``` C++
static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *tempStr;
  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }

  char fpath[1000];

  sprintf(fpath, "%s%s", dirPath, path);
  int res = lstat(fpath, stbuf);

  if (res == -1)
    return -errno;

  return 0;
}
```
> Fungsi ini berfungsi untuk mendapatkan attribute dari file

- Fungsi mkdir
``` C++
static int xmp_mkdir(const char *path, mode_t mode)
{
  if (strstr(path, "Animeku_") != NULL)
  {
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
      path = dirPath;
      sprintf(fpath, "%s", path);
    }
    else
      sprintf(fpath, "%s%s", dirPath, path);
    int result = mkdir(fpath, mode);
    char *folderPath = strstr(path, "Animeku_");
    if (folderPath != NULL)
      logForWibu(fpath, fpath, 0);
    if (result == -1)
      return -errno;
  }
  return 0;
}
```
> Fungsi ini berfungsi untuk membuat direktori

- Fungsi rename
``` C++
static int xmp_rename(const char *sumber, const char *tujuan)
{
  int flag;
  if (strstr(sumber, "Animeku_") != NULL)
    flag = 1;

  char filesrc[1000], filegoal[1000];
  sprintf(filesrc, "%s%s", dirPath, sumber);
  sprintf(filegoal, "%s%s", dirPath, tujuan);
  int result;
  if (flag == 1)
  {
    if (strstr(filegoal, "Animeku_") != NULL)
      logForWibu(filesrc, filegoal, 1);
    else if (strstr(filesrc, "Animeku_") != NULL)
      logForWibu(filesrc, filegoal, 2);
    result = rename(filesrc, filegoal);
  }

  if (result == -1)
    return -errno;
  return 0;
}
```
> Fungsi untuk mengganti file / direktori

- Fungsi open
``` C++
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
  char *tempStr;
  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }
  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirPath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirPath, path);

  int result;
  result = open(fpath, fi->flags);
  if (result == -1)
    return -errno;
  close(result);
  return 0;
}
```
> Fungsi untuk membuka file / direktori

- Fungsi read
``` C++
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char *tempStr;
  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirPath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirPath, path);

  int result = 0;
  int fd = 0;
  (void)fi;

  fd = open(fpath, O_RDONLY);
  if (fd == -1)
    return -errno;

  result = pread(fd, buf, size, offset);
  if (result == -1)
    result = -errno;

  close(fd);
  return result;
}
```
> Membaca potongan demi potongan data dari suatu file

- Fungsi write
``` C++
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char *tempStr;
  if ((tempStr = strstr(path, "Animeku_")) != NULL)
  {
    if (tempStr != NULL)
      decodeForWibu(tempStr);
  }

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirPath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirPath, path);

  int result;
  int fd;
  (void)fi;

  fd = open(fpath, O_WRONLY);
  if (fd == -1)
    return -errno;

  result = pwrite(fd, buf, size, offset);
  if (result == -1)
    result = -errno;
  close(fd);
  return result;
}
```
> Fungsi untuk menulis nama file

- Fungsi decode dan encode
``` C++
void encodeForWibu(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
    return;
  for (int i = 0; i < strlen(str); i++)
  {
    if (str[i] == '/')
      continue;
    if (str[i] == '.')
      break;

    if (str[i] >= 'A' && str[i] <= 'Z')
      str[i] = 'Z' + 'A' - str[i];
    if (str[i] >= 'a' && str[i] <= 'z')
      str[i] = 'a' + (str[i] - 'a' + 13) % 26;
  }
}

void decodeForWibu(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL)
    return;
  int s = 0, length = strlen(str);
  for (int i = length; i >= 0; i--)
  {
    if (str[i] == '/')
      break;
    if (str[i] == '.')
    {
      length = i;
      break;
    }
  }
  for (int i = 0; i < length; i++)
  {
    if (str[i] == '/')
    {
      s = i + 1;
      break;
    }
  }
  for (int i = s; i < length; i++)
  {
    if (str[i] == '/')
      continue;
    if (str[i] >= 'A' && str[i] <= 'Z')
      str[i] = 'Z' + 'A' - str[i];
    if (str[i] >= 'a' && str[i] <= 'z')
      str[i] = 'a' + (str[i] - 'a' + 13) % 26;
  }
}
```

> Fungsi ini digunakan untuk me-encode dan decode nama file

- Fungsi LogWibu
``` C++
void logForWibu(const char *lama, char *baru, int tipe)
{
  char logPath[100];
  char *logName = "Wibu.log";
  sprintf(logPath, "%s/%s", dirPath, logName);
  FILE *logFile = fopen(logPath, "a");

  if (tipe == 0)
    fprintf(logFile, "MKDIR\tterenkripsi\t%s\t->\t%s\n", lama, baru);
  else if (tipe == 1)
    fprintf(logFile, "RENAME\tterenkripsi\t%s\t->\t%s\n", lama, baru);
  else if (tipe == 2)
    fprintf(logFile, "RENAME\tterdecode\t%s\t->\t%s\n", lama, baru);
  fclose(logFile);
}
```
> Fungsi ini file log
# Soal 2

## Persoalan
- Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).
- Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
- Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.
- Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.
- Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 

[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]


## Penyelesaian
> Kami belum bisa menyelesaikan nomor ini karena belum menemukan solusi untuk menjawabnya.

# Soal 3

## Persoalan
- Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial
- Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.
- Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.
- Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).
- Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.
	Contoh : jika pada direktori asli namanya adalah “isHaQ_KEreN.txt” maka pada fuse akan menjadi “ISHAQ_KEREN.txt.1670”. 1670 berasal dari biner 11010000110

## Penyelesaian
> Kami belum bisa menyelesaikan nomor ini karena belum menemukan solusi untuk menjawabnya.